import sqlite3
from json2html import json2html
from modules.htmltemplate import html_template

class SqliteWriter:
    def __init__(self):
        self.__path =  './results.db'
        self.__connection = sqlite3.connect(self.__path)

    def write(self, dataframe):
        dataframe.to_sql('results', self.__connection, if_exists='append', index=False)

class JsonWriter:
    def __init__(self):
        self.__path = './data.json'

    def write(self, dataframe):
        dataframe.to_json(self.__path, orient='records')

class HtmlWriter:
    def write(self, dataframe):
        data = dataframe.to_json(orient='records')
        html_table = json2html.convert(data)
        text_file = open('index.html', 'w')
        text_file.write(html_template.format(html_table))
        text_file.close()

class WriterFactory():
    def create_writer(self, writer_type):
        switch_table = {
            'json': JsonWriter,
            'sqlite': SqliteWriter,
            'html': HtmlWriter,
        }
        return switch_table.get(writer_type)()

