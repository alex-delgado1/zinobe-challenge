import pandas as pd
import hashlib
import datetime

class InformationTable():
    def __init__(self):
        self.__rows = list()

    def __getRows(self):
        return self.__rows

    def addRow(self, row):
        return self.__rows.append(row)

    def toDataFrame(self):
        return pd.concat(self.__getRows(), ignore_index =False)

    def getStatistics(self):
        dataFrame = self.toDataFrame()
        time_column = dataFrame['Time in miliSeconds']
        statistics = {
            'max_time': [f'{time_column.max()} ms'],
            'min_time': [f'{time_column.min()} ms'],
            'total_time': [f'{time_column.sum()} ms'],
            'avg_time': [f'{time_column.mean()} ms'],
            'created_at': [f'{datetime.datetime.now()}'],
        }
        return pd.DataFrame.from_dict(statistics)

class Director():
    __builder = None

    def setBuilder(self, builder):
        self.__builder = builder

    def construct(self, data):
        informationTable = InformationTable()

        for country in data:
            row = self.__builder.build_row(country)
            informationTable.addRow(row)

        return informationTable

class InformationTableBuilder():
    def __hash(self, language):
        return hashlib.sha1(language.encode()).hexdigest()

    def __getElapsedTime(self, start_time, end_time):
        return (end_time - start_time).total_seconds() * 100

    def build_row(self, country):
        start_time = datetime.datetime.now()
        language = country['languages'][0]['name']

        data = {
            'Region': [country['region']],
            'City Name': [country['name']],
            'Language': [self.__hash(language)]
        }
        row = pd.DataFrame.from_dict(data)
        end_time = datetime.datetime.now()
        row['Time in miliSeconds'] = self.__getElapsedTime(start_time, end_time)
        return row
