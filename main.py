import requests

from modules.informationtable  import InformationTableBuilder, Director
from modules.writers import WriterFactory

def main():
    apiURL = 'https://restcountries.com/v2/all'
    data = requests.get(apiURL).json()

    director = Director()
    builder = InformationTableBuilder()
    director.setBuilder(builder)
    informationTable = director.construct(data)

    writer = WriterFactory()
    jsonWriter = writer.create_writer('json')
    sqliteWriter = writer.create_writer('sqlite')
    htmlWriter = writer.create_writer('html')

    jsonWriter.write(informationTable.toDataFrame())
    sqliteWriter.write(informationTable.getStatistics())
    htmlWriter.write(informationTable.toDataFrame())

if __name__ == "__main__":
    main()
