# Zinobe Challenge
## Instalacion
```bash
git clone https://alex-delgado1-admin@bitbucket.org/alex-delgado1/zinobe-challenge.git
cd zinobe-challenge

# Para prevenir conflictos entre dependencias es recomendable crear un ambiente virtual
python -m venv venv
. venv/bin/activate

# Instalar dependencias dentro del ambiente virtual
pip install -r ./requirements.txt
```

## Uso
```bash
python main.py
```

Se generaran los siguientes archivos una vez finalizado el comando anterior:
- data.json 
- results.db
- index.html

index.html contiene una representacion grafica de la tabla generada, que puede ser accedida desde cualquier navegador

## Requisitos del proyecto
|  Region | City Name |  Languaje | Time  |
|---|---|---|---|
|  Africa | Angola  |  AF4F4762F9BD3F0F4A10CAF5B6E63DC4CE543724 | 0.23 ms  |
|   |   |   |   |
|   |   |   |   |

Desarrolle una aplicacion en python que genere la tabla anterior teniendo las siguientes consideraciones:

- De https://restcountries.com/ obtenga el nombre del idioma que habla el pais y encriptelo con SHA1
- En la columna Time ponga el tiempo que tardo en armar la fila (debe ser automatico)
- La tabla debe ser creada en un DataFrame con la libreria PANDAS
- Con funciones de la libreria pandas muestre el tiempo total, el tiempo promedio, el tiempo minimo y el maximo que tardo en procesar toda las filas de la tabla.
- Guarde el resultado en sqlite.
- Genere un Json de la tabla creada y guardelo como data.json
- La prueba debe ser entregada en un repositorio git.



**Es un plus si:**
* No usa famework
* Entrega Test Unitarios
* Presenta un diseño de su solucion.
